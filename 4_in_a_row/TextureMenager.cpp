#include "TextureMenager.h"
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <iostream>
#include <fstream>
#include "Texture.h"
#include "Game.h"

TextureMenager::TextureMenager()
{
}


TextureMenager::~TextureMenager()
{
}

bool TextureMenager::init()
{
	//init data for creating text textures
	mFontFile = "assets/font.ttf";
	mTextManager = std::make_unique<TextTextureManager>();
	mTextManager->loadFont(mFontFile, 72);

	return true;
}


bool TextureMenager::loadTexture(std::string fileName, std::string
	id, std::shared_ptr<SDL_Renderer> pRenderer, SDL_Color *transparentColor)
{
	//checks if texture already exist
	auto it= mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		std::cout << "texture already loaded \t" 
							<< fileName << "\n";
		return false;
	}

	SDL_Surface* pTempSurface = IMG_Load(fileName.c_str());
	if (pTempSurface == NULL)
	{
		std::cout << "couldnt load image \t" << 
						SDL_GetError() << fileName << "\n";
		return false;
	}
	//setting transparent color
	if (transparentColor != nullptr)
	{
		SDL_SetColorKey(pTempSurface, SDL_TRUE, SDL_MapRGB
			(pTempSurface->format, transparentColor->r, 
			transparentColor->g, transparentColor->b));
	}

	SDL_Texture* pTexture =
		SDL_CreateTextureFromSurface(pRenderer.get(), pTempSurface);
	
	// everything went ok, add the texture to our list
	if (pTexture != 0)
	{
		std::cout << "texture loaded \t" << fileName << "\n";
		
		mTextureMap[id] = std::make_unique<Texture>();
		std::cout << "Texture height: " << pTempSurface->h << "\n";
		
		mTextureMap[id]->load(pTexture, 
			pTempSurface->w, pTempSurface->h);
		SDL_FreeSurface(pTempSurface);
		return true;
	}

	SDL_FreeSurface(pTempSurface);
	// reaching here means something went wrong
	std::cout << "couldnt create texture \t" << 
						fileName << SDL_GetError() <<"\n";
	return false;
}

bool TextureMenager::loadSpriteShit(std::string fileName, std::string id, 
	std::shared_ptr<SDL_Renderer> pRenderer, SDL_Color * transparentColor, std::string metaData)
{
	//checks if texture already exist
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		std::cout << "texture already loaded \t"
			<< fileName << "\n";
		return false;
	}

	SDL_Surface* pTempSurface = IMG_Load(fileName.c_str());
	if (pTempSurface == NULL)
	{
		std::cout << "couldnt load image \t" <<
			SDL_GetError() << fileName << "\n";
		return false;
	}
	//setting transparent color
	if (transparentColor != nullptr)
	{
		SDL_SetColorKey(pTempSurface, SDL_TRUE, SDL_MapRGB
			(pTempSurface->format, transparentColor->r,
				transparentColor->g, transparentColor->b));
	}

	SDL_Texture* pTexture =
		SDL_CreateTextureFromSurface(pRenderer.get(), pTempSurface);

	// everything went ok, add the texture to our list
	if (pTexture != 0)
	{
		std::cout << "texture loaded \t" << fileName << "\n";

		mTextureMap[id] = std::make_unique<Texture>();
		std::cout << "Texture height: " << pTempSurface->h << "\n";
		
		//load frames meta data
		std::vector<std::vector<SDL_Rect>> frameData;
		loadMetaData(metaData, frameData);

		mTextureMap[id]->load(pTexture,
			pTempSurface->w, pTempSurface->h, frameData);
		SDL_FreeSurface(pTempSurface);
		return true;
	}

	SDL_FreeSurface(pTempSurface);
	// reaching here means something went wrong
	std::cout << "couldnt create texture \t" <<
		fileName << SDL_GetError() << "\n";
	return false;
}


bool TextureMenager::createTextTexture(std::string textureText, 
	SDL_Color textColor, std::shared_ptr<SDL_Renderer> pRenderer)
{
	//checks if texture already exist
	auto it = mTextureMap.find(textureText);
	if (it != mTextureMap.end())
	{
		std::cout << "texture already loaded \t"
			<< textureText << "\n";
		return false;
	}

	mTextureMap[textureText] = mTextManager->createTextTexture(textureText,
		textColor, pRenderer.get());
		
	return mTextureMap[textureText] != nullptr;
}


bool TextureMenager::createBackgroundTexture(std::string ID, SDL_Color color,
	std::shared_ptr<SDL_Renderer> pRenderer, SDL_BlendMode blending, Uint8 alpha)
{
	auto it = mTextureMap.find(ID);
	if (it != mTextureMap.end())
	{
		std::cout << "background texture already loaded \t"
			<< ID << "\n";
		return false;
	}

	//create blank texture
	SDL_Texture* pTexture = SDL_CreateTexture(pRenderer.get(), 
		SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 
		Game::Instance()->getGameWidth(), Game::Instance()->getGameHight());
	if (pTexture == NULL)
	{
		std::cout << "Unable to create blank texture! SDL Error: " <<
			SDL_GetError() << "\n";
		return false;
	}

	//set render tareget to created texture
	SDL_SetRenderTarget(pRenderer.get(), pTexture);
	SDL_Rect fillRect = { 0, 0, Game::Instance()->getGameWidth(),
		Game::Instance()->getGameHight() };
	SDL_SetRenderDrawColor(Game::Instance()->getRenderer().get(),
		color.r, color.g, color.b, color.a);
	SDL_RenderFillRect(Game::Instance()->getRenderer().get(), &fillRect);
	SDL_SetRenderDrawColor(Game::Instance()->getRenderer().get(), 0, 0, 0, 255);
	//Reset render target
	SDL_SetRenderTarget(pRenderer.get(), NULL);

	//add created texure to texures map
	mTextureMap[ID] = std::make_unique<Texture>();
	mTextureMap[ID]->load(pTexture, Game::Instance()->getGameWidth(),
		Game::Instance()->getGameHight());
	mTextureMap[ID]->setBlendMode(blending);
	mTextureMap[ID]->setAlpha(alpha);

	return true;
}


void TextureMenager::draw(std::string id, SDL_Rect srcRect, SDL_Rect destRect,
	std::shared_ptr<SDL_Renderer> pRenderer, SDL_RendererFlip flip)
{
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		SDL_RenderCopyEx(pRenderer.get(), mTextureMap[id]->getTexure(), &srcRect,
			&destRect, 0, 0, flip);
	}
	else
	{
		std::cout << "No such texture: " << id << " Cant draw\n";
	}
}


//draw single frame
void TextureMenager::drawFrame(std::string id, SDL_Rect destRect, int frame, int row,
	std::shared_ptr<SDL_Renderer> pRenderer, SDL_RendererFlip flip)
{
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		SDL_Rect srcRect = mTextureMap[id]->getFrameRect(row, frame);
		//set dest rect to match current frame dimension
		int dy = destRect.h - srcRect.h;
		destRect.y += dy;
		destRect.h -= dy;
		int dx = destRect.w - srcRect.w;
		destRect.x += dx / 2;
		destRect.w -= dx;


		SDL_RenderCopyEx(pRenderer.get(), mTextureMap[id]->getTexure(), &srcRect,
			&destRect, 0, 0, flip);
	}
	else
	{
		std::cout << "No such texture: " << id << " Cant draw\n";
	}
}

void TextureMenager::drawFrameScaled(std::string id, SDL_Rect destRect, int frame, 
	int row, std::shared_ptr<SDL_Renderer> pRenderer, SDL_RendererFlip flip)
{
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		SDL_Rect srcRect = mTextureMap[id]->getFrameRect(row, frame);
		
		SDL_RenderCopyEx(pRenderer.get(), mTextureMap[id]->getTexure(), &srcRect,
			&destRect, 0, 0, flip);
	}
	else
	{
		std::cout << "No such texture: " << id << " Cant draw\n";
	}
}


void TextureMenager::modulateColor(std::string id, Uint8 red, Uint8 green, Uint8 blue)
{
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		it->second->modulateColor(red, green, blue);
	}
	else
	{
		std::cout << "No such texture: " << id << " Cant modulate color\n";
	}
}

void TextureMenager::clean()
{
	mTextureMap.clear();
	mTextManager->clear();
}


void TextureMenager::removeTexture(std::string id)
{
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		mTextureMap.at(id)->clean();
		mTextureMap.erase(id);
	}
	else
	{
		std::cout << "No such texture: " << id
			<< " Cant remove texture\n";
	}

}

bool TextureMenager::loadMetaData(std::string fileName, 
	std::vector<std::vector<SDL_Rect>> &frameData)
{
	//loads rectangle of each frame from external file

	std::ifstream file;
	file.open(fileName, std::ios::in | std::ios::out);
	if (file.good() == false)
	{
		std::cout << "Couldnt load spritesheet metadata: " << fileName << "\n";
		return false;
	}

	int rows;
	int frames;
	std::string desc;
	SDL_Rect r;

	file >> rows;	//nr of animation rows(metadata!! not real nr of frames in spritesheet)
	for (int i = 0; i < rows; ++i)
	{
		frameData.push_back(std::vector<SDL_Rect>());
		file >> frames >> desc;
		for (int j = 0; j < frames; ++j)
		{
			file >> r.x >> r.y >> r.w >> r.h;
			frameData[i].push_back(r);
		}
	}
	file.close();
	return true;
}

//return dimensions of given texture
std::pair<int, int> 
	TextureMenager::getDimensions(std::string id)
{
	auto it = mTextureMap.find(id);
	if (it != mTextureMap.end())
	{
		return mTextureMap[id]->getDimensions();
	}
	else
	{
		std::cout << "No such texture: " << id 
			<< " Cant return dimensions\n";
		return std::pair<int, int>(0, 0);
	}
	
}