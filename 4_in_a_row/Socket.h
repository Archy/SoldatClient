#pragma once
#define WIN32_LEAN_AND_MEAN 
#include <WinSock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

class Socket
{
public:
	bool readData(char * data, int & size, char * senderIP, USHORT & port);	//read data that has been sent
	void getLocalIP(char *localIP);	//read local IPv4

protected:
	Socket();
	~Socket();

	bool init(int port);
	void close();

protected:
	SOCKET mSock;

	//server adress
	SOCKADDR_IN mRemoteAddr;
	//socket adress
	SOCKADDR_IN mLocalAddr;

	bool mInitialized;

	const static int sIP_SIZE = 16;	// size of "nnn.nnn.nnn.nnn" for getting ip

private:
	WSADATA mWSAData;
};

