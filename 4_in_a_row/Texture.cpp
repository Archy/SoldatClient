#include "Texture.h"


Texture::Texture() :
	mHeight(0), mWidth(0)
{
}


Texture::~Texture()
{
	SDL_DestroyTexture(mTexture);
}


void Texture::load(SDL_Texture* texture, 
	int width, int height)
{
	//load texure
	mTexture = texture;
	mHeight = height;
	mWidth = width;
}

void Texture::load(SDL_Texture * texture, int width, int height,
		std::vector<std::vector<SDL_Rect>> frameData)
{
	//load texure and frame data
	mFrameData = frameData;	
	load(texture, width, height);
}

void Texture::clean()
{
}


void Texture::setBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(mTexture, blending);
}

void Texture::setAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void Texture::modulateColor(Uint8 red, Uint8 green, Uint8 blue)
{
	//Modulate texture
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}


std::pair<int, int> Texture::getDimensions()
{
	std::pair<int, int>dimPair;
	dimPair.first = mWidth;
	dimPair.second = mHeight;

	return dimPair;
}