#pragma once
#include<math.h>

class Vector2D
{
public:
	Vector2D(float x = 0.0, float y = 0.0) : mX(x), mY(y) {}
	~Vector2D(){}


	//get/set function
	float getX() const { return mX; }
	float getY() const { return mY; }

	void setX(float x) { mX = x; }
	void setY(float y) { mY = y; }


	//basic 2D vector operations
	float length() const { return static_cast<float>(sqrt(mX * mX + mY * mY)); }

	Vector2D Vector2D::operator+(const Vector2D& v2) const;
	Vector2D& operator+=(const Vector2D& v2);

	Vector2D operator*(float scalar) const;
	Vector2D& operator*=(float scalar);

	Vector2D operator-(const Vector2D& v2) const;
	Vector2D operator-=(const Vector2D& v2);

	Vector2D operator/(float scalar) const;
	Vector2D& operator/=(float scalar);

	//make vector lenght equal to 1
	//->it only represents direction
	void normalize();
private:
	float mX;
	float mY;
};

