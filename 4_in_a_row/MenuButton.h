#pragma once
#include "GameObject.h"
#include <utility>

class MenuButton final :
	public GameObject
{
public:
	~MenuButton();
	MenuButton();

	virtual void load(std::unique_ptr<LoaderParams> const &pParams);

	virtual void draw();
	virtual void update();
	virtual void clean();

	//function used to set callback function
	int getCallbackID(){ return mCallbackID; }
	void setCallback(void (*callback)()){ m_callback = callback; }

private:
	std::string mText;

	bool mMouseOver;
	bool mClicked;

	//button callback function data
	int mCallbackID;
	void(*m_callback)();
};