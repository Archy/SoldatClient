#include "MainMenuState.h"
#include "GameObject.h"
#include "LoaderParams.h"
#include "Game.h"
#include <memory>
#include <utility>
#include <iostream>

#include "AnimatedBackground.h"
#include "MenuButton.h"


MainMenuState::MainMenuState(const int stateID) : 
	MenuState(stateID)
{
}

MainMenuState::~MainMenuState()
{
}


bool MainMenuState::EnterState()
{
// TEXTURES
	SDL_Color transparent{ 255, 255, 255, 255 };
	
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/menubutton.png", "button",
		Game::Instance()->getRenderer(), &transparent))
	{
		mTextureIDList.push_back("button"); // push into list
	}
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/clouds.png", "cloud",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("cloud"); // push into list
	}

// TEXTTEXTURES
	SDL_Color textColor {255, 255, 0 ,255};
	if (Game::Instance()->getTextureMenager()->createTextTexture(
		"Play", textColor, Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("Play"); // push into list
	}

	textColor = SDL_Color{ 0, 255, 0 ,255 };
	if (Game::Instance()->getTextureMenager()->createTextTexture(
		"Exit", textColor, Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("Exit"); // push into list
	}

// OBJECTS
	std::shared_ptr<AnimatedBackground> background = std::make_shared<AnimatedBackground>();
	background->load(std::unique_ptr<LoaderParams>(new LoaderParams(0, 0,
		Game::Instance()->getGameWidth(), Game::Instance()->getGameHight(), "cloud", 1, 30)));
	mStateObjects.push_back(background);

	std::shared_ptr<MenuButton> button1 = std::make_shared<MenuButton>();
	button1->load(std::unique_ptr<LoaderParams>(new LoaderParams(100, 100,
		400, 100, "button", "Play", 0)));
	mStateObjects.push_back(button1);

	std::shared_ptr<MenuButton> button2 = std::make_shared<MenuButton>();
	button2->load(std::unique_ptr<LoaderParams>(new LoaderParams(100, 300,
		400, 100, "button", "Exit", 1)));
	mStateObjects.push_back(button2);


//finish
	std::cout << "entering MainMenuState\n";

	pushCallbacks();
	assignCallbacks();

	return true;
}


bool MainMenuState::ExitState()
{
	std::cout << "exiting MainMenuState\n";
	return true;
}


void MainMenuState::pushCallbacks()
{
	mCallbacks.push_back(MainMenuState::playFunction);
	mCallbacks.push_back(MainMenuState::exitFunction);
}

//buttons callback functions

void MainMenuState::exitFunction()
{
	Game::Instance()->quit();
	std::cout << "exit Function called\n";
}

void MainMenuState::playFunction()
{
	std::cout << "play Function called\n";
	Game::Instance()->getStateMachine()->changeState(StateMachine::PlayState);
}