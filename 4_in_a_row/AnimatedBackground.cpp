#include "AnimatedBackground.h"
#include "LoaderParams.h"
#include "Game.h"
#include <SDL.h>



AnimatedBackground::AnimatedBackground()
{
}


AnimatedBackground::~AnimatedBackground()
{
}


void AnimatedBackground::load(std::unique_ptr<LoaderParams> const &pParams)
{
	//load additional data for animation
	mAnimSpeed = pParams->getAnimSpeed();
	GameObject::load(pParams);

	std::pair<int, int>dimension = 
		Game::Instance()->getTextureMenager()->getDimensions(mTextureID);
	
	mTextureWidth = dimension.first;
	mTextureHeight = dimension.second;

	resetRect();
	mTicksPrev = SDL_GetTicks();
}


void AnimatedBackground::draw()
{
	//rect for left part
	SDL_Rect leftRectSrc{ static_cast<int>(mLeftRect.srcX), 0,
		static_cast<int>(mLeftRect.srcWidth), mTextureHeight };
	SDL_Rect leftRectDest{ static_cast<int>(mLeftRect.destX), 0,
		static_cast<int>(mLeftRect.destWidth), mTextureHeight };

	//rects for right part
	SDL_Rect rightRectSrc{ static_cast<int>(mRightRect.srcX), 0,
		static_cast<int>(mRightRect.srcWidth), mTextureHeight };
	SDL_Rect rightRectDest{ static_cast<int>(mRightRect.destX), 0,
		static_cast<int>(mRightRect.destWidth), mTextureHeight };

	Game::Instance()->getTextureMenager()->draw(
		mTextureID, leftRectSrc, leftRectDest, 
		Game::Instance()->getRenderer());
	Game::Instance()->getTextureMenager()->draw(
		mTextureID, rightRectSrc, rightRectDest, 
		Game::Instance()->getRenderer());

}

void AnimatedBackground::update()
{
	mTicksActual = SDL_GetTicks();

	//left is width is shrinking and moving right
	mLeftRect.destWidth -= shift();
	mLeftRect.srcX += shift();
	mLeftRect.srcWidth -= shift();

	//right width is extending and moving left
	mRightRect.destX -= shift();
	mRightRect.destWidth += shift();
	mRightRect.srcWidth += shift();

	if (mRightRect.destX <= 0)
	{
		resetRect();
	}
	mTicksPrev = mTicksActual;
}

void AnimatedBackground::clean()
{

}


void AnimatedBackground::resetRect()
{
	//left rect is covering all scr and dest
	mLeftRect.destX = 0.0; //const
	mLeftRect.destWidth = static_cast<float>(mTextureWidth);
	mLeftRect.srcX = 0.0;
	mLeftRect.srcWidth = static_cast<float>(mTextureWidth);

	//right rect covers nothing
	mRightRect.destX = static_cast<float>(mWidth);
	mRightRect.destWidth = 0.0;
	mRightRect.srcX = 0.0;	//const
	mRightRect.srcWidth = 0.0;
}


float AnimatedBackground::shift()
{
	//movement since prev calling of update function
	return static_cast<float>(
		(mAnimSpeed*((mTicksActual - 
			mTicksPrev)))) / 1000;
}