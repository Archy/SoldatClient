#pragma once
#include <SDL.h>

class Timer
{
public:
	Timer();
	~Timer();

	void start();
	//count how much time has passed since previous call
	void update();

	//get time that has passed from previous frame
	float getDT();

private:
	Uint32 mPrevTime;
	Uint32 mTime;
};

