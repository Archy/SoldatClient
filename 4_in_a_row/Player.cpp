#include "Player.h"
#include "Game.h"


Player::Player() : 
	LEFT_KEY(SDL_SCANCODE_A), RIGHT_KEY(SDL_SCANCODE_D),
	JUMP_KEY(SDL_SCANCODE_W), SHOOT_KEY(SDL_SCANCODE_SPACE),
	mState(Soldier::SoldierState::STAY), mAction(Soldier::SoldierAction::NONE),
	mDir(Soldier::SoldierDir::RIGHT)
{
}


Player::~Player()
{
}

void Player::update()
{
	//process user input and set players state 
	std::shared_ptr<InputHandler> in;
	in = Game::Instance()->getInputHandler();

	if (in->isKeyDown(SHOOT_KEY))
	{
		mAction = Soldier::SoldierAction::SHOOT;
	}
	else if (in->isKeyDown(JUMP_KEY))
	{
		mAction = Soldier::SoldierAction::JUMP;
	}
	else
	{
		mAction = Soldier::SoldierAction::NONE;
	}

	if(in->isKeyDown(RIGHT_KEY))
	{
		mState = Soldier::SoldierState::MOVE;
		mDir = Soldier::SoldierDir::RIGHT;
	}
	else if (in->isKeyDown(LEFT_KEY))
	{
		mState = Soldier::SoldierState::MOVE;
		mDir = Soldier::SoldierDir::LEFT;
	}
	else
	{
		mState = Soldier::SoldierState::STAY;
	}
}

void Player::draw()
{
//dont draw dead players info
	if (mLives <= 0)
		return;

//draw remaining bullets
	SDL_Rect dest{ 0, Game::Instance()->getGameHight() - mHeight, mWidth, mHeight };
	//if player is reloading
	if (mAmmo <= 0)
	{
		dest.w *= 4;
		SDL_Rect src { 0, 0, 
			Game::Instance()->getTextureMenager()->getDimensions("reload").first,
			Game::Instance()->getTextureMenager()->getDimensions("reload").second };
		Game::Instance()->getTextureMenager()->draw("reload", src, dest, 
			Game::Instance()->getRenderer());
		dest.w = mWidth;
	}
	else
	{
		for (int i = 0; i < mAmmo; ++i)
		{
			Game::Instance()->getTextureMenager()->drawFrameScaled(mTextureID, dest,
				3, 5, Game::Instance()->getRenderer());
			dest.x += dest.w;
		}
	}

//draw remaining lives
	//set soldier color modulation
	switch (mNumber)
	{
	case 0:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 255, 215, 0);
		break;
	case 1:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 255, 75, 75);
		break;
	case 2:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 25, 255, 25);
		break;
	case 3:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 75, 75, 255);
		break;
	default:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 0, 0, 0);
		break;
	}

	
	dest.x = Game::Instance()->getGameWidth() - MAX_LIVES * mWidth;
	for (int i = 0; i < mLives; ++i)
	{
		Game::Instance()->getTextureMenager()->drawFrameScaled(mTextureID, dest,
			0, 0, Game::Instance()->getRenderer());

		dest.x += dest.w;
	}

	//reset texture color modulation
	Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 255, 255, 255);
}

void Player::clean()
{
}


char Player::getStateToSend()
{
	//prepare user state for sending to server
	char a = 0;

	if (mDir == Soldier::SoldierDir::RIGHT)
		a |= 0x20;

	switch (mState)
	{
	case Soldier::MOVE:
		a |= 0x08;
		break;
	case Soldier::STAY:
		break;
	case Soldier::DIE:
		a |= 0x18;
		break;
	default:
		break;
	}

	switch (mAction)
	{
	case Soldier::JUMP:
		a |= 0x02;
		break;
	case Soldier::FALL:
		a |= 0x04;
		break;
	case Soldier::NONE:
		break;
	case Soldier::SHOOT:
		a |= 0x06;
		break;
	default:
		break;
	}

	return a;
}

void Player::setInfo(char a)
{
	char nr = a;
	nr >>= 6;
	nr &= 0x03;
	mNumber = nr;

	//reset numbers bits
	a &= 0x3f;

	char ammo = a;
	ammo >>= 3;
	ammo &= 0x03;
	mAmmo = ammo;

	char lives = a;
	lives &= 0x03;
	mLives = lives;
}
