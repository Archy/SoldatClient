#pragma once
#include "Vector2D.h"
#include <string>
#include <memory>
#include <iostream>

class LoaderParams;

//pure virtual class - base of every game object
class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	//load basic object data(position, width, height, etc)
	virtual void load(std::unique_ptr<LoaderParams> const &pParams);

	virtual void draw();
	virtual void update() = 0;
	virtual void clean() = 0;

protected:
	//basic data of EVERY game object
	Vector2D mPosition;

	int mWidth;
	int mHeight;

	std::string mTextureID;
};

