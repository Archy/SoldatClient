#pragma once
#include "Soldier.h"
#include <SDL.h>

class Player : 
	public GameObject
{
public:
	Player();
	~Player();

	virtual void update();
	virtual void draw();
	virtual void clean();

	char getStateToSend();

	void setInfo(char a);

private:
	//players states
	Soldier::SoldierState mState;
	Soldier::SoldierAction mAction;
	Soldier::SoldierDir mDir;
	
	//players soldier data
	char mNumber;
	char mLives;
	char mAmmo;

private:
	//keys used by player
	const SDL_Scancode LEFT_KEY;
	const SDL_Scancode RIGHT_KEY;
	const SDL_Scancode JUMP_KEY;
	const SDL_Scancode SHOOT_KEY;

	static const int MAX_AMMO = 3;
	static const int MAX_LIVES = 4;
};

