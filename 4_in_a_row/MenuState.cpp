#include "MenuState.h"
#include "MenuButton.h"
#include "Game.h"



MenuState::MenuState(int stateID) : 
	GameState(stateID)
{
}


MenuState::~MenuState()
{
}


void MenuState::update()
{
	for (auto& it : mStateObjects)
	{
		it->update();
	}
}

void MenuState::render()
{
	for (auto& it : mStateObjects)
	{
		it->draw();
	}
}


void MenuState::clear()
{
	for (auto& it : mStateObjects)
	{
		it->clean();
	}
	mStateObjects.clear();
}

void MenuState::clearData()
{
	for (auto& it : mTextureIDList)
	{
		Game::Instance()->getTextureMenager()->removeTexture(it);
	}
}


void MenuState::assignCallbacks()
{
	for (auto& it: mStateObjects)
	{
		//asing callback function to every button
		if (std::dynamic_pointer_cast<MenuButton>(it))
		{
			std::shared_ptr<MenuButton> button = std::dynamic_pointer_cast<MenuButton>(it);

			button->setCallback(mCallbacks
				[button->getCallbackID()]);

			std::cout << "callback function assigned: "
				<< button->getCallbackID() << "\n";
		}
	}
}