#pragma once
#include "MenuState.h"
#include "GameStateCreator.h"

class GameOverState :
	public MenuState
{
public:
	~GameOverState();

	virtual bool EnterState();
	virtual bool ExitState();

private:
	friend class GameOVerStateCreator;
	GameOverState(int stateID);

	//add callbacks functions to callbacks vector
	virtual void pushCallbacks();

	//callback functions
	static void restartFunction();
	static void exitFunction();
};

class GameOVerStateCreator : public GameStateCreator
{
	virtual GameState* createGameObject(const int id) const
	{
		return new GameOverState(id);
	}
};