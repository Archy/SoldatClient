#include "Socket.h"
#include <iostream>

/*
htons - 16 bit integer value from host byte order (little or big-endian)
to network byte order (big-endian)
htonl - host to network long
*/

bool Socket::readData(char * data, int & size, char * senderIP, USHORT & port)
{
	int result;
	int remoteAddrSize;
	remoteAddrSize = sizeof(mRemoteAddr);

	result = recvfrom(mSock, data, size, 0, (SOCKADDR *)&mRemoteAddr,
		&remoteAddrSize);
	if (result == SOCKET_ERROR)	//error while receiving data
	{
		size = 0;
		return false;
	}
	else
	{
		inet_ntop(AF_INET, &mRemoteAddr.sin_addr, senderIP, 16);	//get sender IP
		port = mRemoteAddr.sin_port;		// port number of sender
		size = result;
		return true;
	}
}

Socket::Socket() :
	mInitialized(false), mSock(NULL)
{
}


Socket::~Socket()
{
}

bool Socket::init(int port)
{
	//init winsock
	int status;
	if (mInitialized) // If network currently initialized
		this->close(); // Close current network and start over

	status = WSAStartup(
		MAKEWORD(2, 2),			// Initiate the use of winsock 2.2
		&mWSAData);		// receives data about winsock implementation
	if (status != 0)
	{
		std:: cout << "Failed. Error Code: " << WSAGetLastError();
		return false;	//Winsock init failure
	}

	//create UDP socket
	mSock = socket(
		AF_INET,		//adress family - IPv4
		SOCK_DGRAM,		//supports datagrams
		IPPROTO_UDP);
	if (mSock == INVALID_SOCKET)
	{
		//socket creation failure
		std::cout << "socket() failed with error code: " << WSAGetLastError();
		WSACleanup();
		return false;
	}

	//put socket in Nonblocking Mode
	unsigned long ul = 1;
	status = ioctlsocket(mSock,
		FIONBIO,
		&ul);	//argp!= NULL => put socket in non blocking mode xD
	if (status == SOCKET_ERROR)
	{
		//putting in nonblocking mode failure
		WSACleanup();
		return false;
	}

	//Set Family and Port
	memset((char *)&mLocalAddr, 0, sizeof(mLocalAddr));
	mLocalAddr.sin_family = AF_INET;
	mLocalAddr.sin_port = htons((u_short)port);    // port number

	memset((char *)&mRemoteAddr, 0, sizeof(mRemoteAddr));
	mRemoteAddr.sin_family = AF_INET;
	mRemoteAddr.sin_port = htons((u_short)port);

	mInitialized = true;
	return true;
}

void Socket::close()
{
	mInitialized = false;
	// closesocket() implicitly causes a shutdown sequence to occur
	if (closesocket(mSock) == SOCKET_ERROR)
	{
		return;
	}

	WSACleanup();
}

//get local IP
void Socket::getLocalIP(char * localIP)
{
	int result;
	char hostName[80];
	addrinfo hints, *res;
	in_addr addr;

	gethostname(hostName, sizeof(hostName));

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_UDP;

	// setup host structure for use in getaddrinfo() function
	result = getaddrinfo(hostName, NULL, &hints, &res);
	if (result != 0) {
		std::cout << "Bad host lookup." << std::endl;
		return;
	}

	// get IP address of server
	addr.s_addr = ((struct sockaddr_in *)(res->ai_addr))->sin_addr.s_addr;
	inet_ntop(AF_INET, &addr.s_addr, localIP, sIP_SIZE);
}
