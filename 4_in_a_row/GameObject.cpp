#include "GameObject.h"
#include "Game.h"
#include "LoaderParams.h"

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

void GameObject::load(std::unique_ptr<LoaderParams> const &pParams)
{
	//load basic data
	mPosition = Vector2D(static_cast<float>(pParams->getX()), 
								static_cast<float>(pParams->getY()));

	mWidth = pParams->getWidth();
	mHeight = pParams->getHeight();
	mTextureID = pParams->getTextureID();

}


void GameObject::draw()
{
	//basic draw
	SDL_Rect srcRect{ static_cast<int>(mPosition.getX()),
			static_cast<int>(mPosition.getY()),
			mWidth, mHeight };
	SDL_Rect destRect{ static_cast<int>(mPosition.getX()),
			static_cast<int>(mPosition.getY()),
			mWidth, mHeight };

	Game::Instance()->getTextureMenager()->draw(mTextureID,
			srcRect, destRect, Game::Instance()->getRenderer());
}