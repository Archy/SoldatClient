#include "InputHandler.h"
#include "Game.h"

InputHandler::~InputHandler()
{}

InputHandler::InputHandler() :
	mKeystates(nullptr),
	mMousePosition(new Vector2D(0, 0))
{
	//mouse buttons, indexes as in enum mouse_buttons
	for (int i = 0; i < 3; i++)
	{
		mMouseButtonStates.push_back(false);
	}
}


void InputHandler::update()
{
	//check if any SDL event ocured
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			Game::Instance()->quit();
			break;
		case SDL_MOUSEMOTION:
			onMouseMove(event);
			break;
		case SDL_MOUSEBUTTONDOWN:
			onMouseButtonDown(event);
			break;
		case SDL_MOUSEBUTTONUP:
			onMouseButtonUp(event);
			break;
		case SDL_KEYDOWN:
			onKeyDown();
			break;
		case SDL_KEYUP:
			onKeyUp();
			break;
		default:
			break;
		}
	}
}


void InputHandler::clean()
{
	// clear arrays
	mMouseButtonStates.clear();
}


void InputHandler::reset()
{
	mMouseButtonStates[LEFT] = false;
	mMouseButtonStates[RIGHT] = false;
	mMouseButtonStates[MIDDLE] = false;
}


bool InputHandler::getMouseButtonState(int buttonNumber) const
{
	return mMouseButtonStates[buttonNumber];
}


std::shared_ptr<const Vector2D> InputHandler::getMousePosition()
{
	return mMousePosition;
}


bool InputHandler::isKeyDown(SDL_Scancode key) const
{
	if (mKeystates != nullptr)
	{
		if (mKeystates[key] == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
}


//###############################################################


void InputHandler::onKeyDown()
{
	mKeystates = SDL_GetKeyboardState(0);
}


void InputHandler::onKeyUp()
{
	mKeystates = SDL_GetKeyboardState(0);
}



void InputHandler::onMouseMove(SDL_Event& event)
{
	mMousePosition->setX(static_cast<float>(event.motion.x));
	mMousePosition->setY(static_cast<float>(event.motion.y));
}


void InputHandler::onMouseButtonDown(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT)
	{
		mMouseButtonStates[LEFT] = true;
	}
	if (event.button.button == SDL_BUTTON_MIDDLE)
	{
		mMouseButtonStates[MIDDLE] = true;
	}
	if (event.button.button == SDL_BUTTON_RIGHT)
	{
		mMouseButtonStates[RIGHT] = true;
	}
}


void InputHandler::onMouseButtonUp(SDL_Event& event)
{
	if (event.button.button == SDL_BUTTON_LEFT)
	{
		mMouseButtonStates[LEFT] = false;
	}
	if (event.button.button == SDL_BUTTON_MIDDLE)
	{
		mMouseButtonStates[MIDDLE] = false;
	}
	if (event.button.button == SDL_BUTTON_RIGHT)
	{
		mMouseButtonStates[RIGHT] = false;
	}
}