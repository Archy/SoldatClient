#pragma once
#include <SDL.h>
#include <utility>
#include <vector>

//wraps single texture
class Texture
{
public:
	Texture();
	~Texture();

	//set Texture instance data
	void load(SDL_Texture* texture, int width, int height);
	//set Texture instance data with frames rectanegles
	void load(SDL_Texture* texture, int width, int height, 
			std::vector<std::vector<SDL_Rect>> frameData);
	void clean();

	//get functions
	SDL_Texture* getTexure(){ return mTexture; }
	int getWidth() const { return mWidth; }
	int getHeight() const { return mHeight; }

	//return texture dimension
	std::pair<int, int> getDimensions();

	//for texture blending
	void setBlendMode(SDL_BlendMode blending);
	void setAlpha(Uint8 alpha);

	//modulate texture color
	void modulateColor(Uint8 red, Uint8 green, Uint8 blue);

	SDL_Rect getFrameRect(int row, int frame) { return mFrameData[row][frame]; }

private:
	int mWidth;
	int mHeight;
	std::vector<std::vector<SDL_Rect>> mFrameData; //data for frames rectangle

	SDL_Texture* mTexture;
};

