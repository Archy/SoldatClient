#pragma once
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <memory>

class GameObject;


//pure virtual class - base of every game state
class GameState
{
public:
	GameState::GameState(const int stateID) : mStateID(stateID)
	{}

	virtual void update() = 0;
	virtual void render() = 0;

	virtual bool EnterState() = 0;	//when state is created
	virtual bool ExitState() = 0; 	//when state is removed

	virtual void clear() = 0;	
	virtual void clearData() = 0;

	int getStateID() const { return mStateID; }
	//void setStateID(int stateID)  { m_pStateID = stateID; }

protected:
	//id given to state by StateMachine
	const int mStateID;

	//used to remove them when exiting the state
	//ids of textures used by GameState instance
	std::vector<std::string> mTextureIDList;

	//vector of object used in state
	std::vector<std::shared_ptr<GameObject>> mStateObjects;
};

