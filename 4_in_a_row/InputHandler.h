#pragma once
#include "SDL.h"
#include <vector>
#include "Vector2D.h"
#include <memory>

//handle and remembers user mouse and keyboard input
class InputHandler
{
public:
	enum mouse_buttons
	{
		LEFT = 0,
		MIDDLE = 1,
		RIGHT = 2
	};

public:
	InputHandler();
	~InputHandler();

	void update();
	void clean();
	void reset();

	//return choosen mouse button state
	bool getMouseButtonState(int buttonNumber) const;
	//returns mouse position as vector
	std::shared_ptr<const Vector2D> getMousePosition();

	//returns true if choosen key is down
	bool isKeyDown(SDL_Scancode key) const;

private:
	// handle keyboard events
	void onKeyDown();
	void onKeyUp();

	// handle mouse events
	void onMouseMove(SDL_Event& event);
	void onMouseButtonDown(SDL_Event& event);
	void onMouseButtonUp(SDL_Event& event);


private:
	//vector for mouse buttons 
	//	0 - left
	//	1 - middle
	//	2 - right
	std::vector<bool> mMouseButtonStates;
	std::shared_ptr<Vector2D> mMousePosition;

	// keyboard specific
	const Uint8* mKeystates;
};