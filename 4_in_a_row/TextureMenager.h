#pragma once
#include <map>
#include <string>
#include <SDL.h>
#include <memory>
#include <vector>
#include <utility>
#include "TextTextureManager.h"

class Texture;

class TextureMenager
{
public:
	TextureMenager();
	~TextureMenager();

	bool init();

	//load file with image to create texture
	bool loadTexture(std::string fileName, std::string id,
		std::shared_ptr<SDL_Renderer>, SDL_Color *transparentColor = nullptr);
	//load file with image to create texture and load frames rectangles from external file
	bool loadSpriteShit(std::string fileName, std::string id,
		std::shared_ptr<SDL_Renderer>, SDL_Color *transparentColor, std::string metaData);

	//create text texture
	bool createTextTexture(std::string textureText, SDL_Color textColor,
		std::shared_ptr<SDL_Renderer>);
	// create texture size of game to render as background
	bool createBackgroundTexture(std::string ID, SDL_Color color,
		std::shared_ptr<SDL_Renderer>, SDL_BlendMode blending, Uint8 alpha);

	// draw singe image
	void draw(std::string id, SDL_Rect srcRect, SDL_Rect destRect, 
		std::shared_ptr<SDL_Renderer>, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//draw single frame of sprite texture
	void drawFrame(std::string id, SDL_Rect destRect, int frame, int r0w, 
		std::shared_ptr<SDL_Renderer>, SDL_RendererFlip flip = SDL_FLIP_NONE);
	//draw single frame of sprite texture scaled to given destination rect
	void drawFrameScaled(std::string id, SDL_Rect destRect, int frame, int r0w,
		std::shared_ptr<SDL_Renderer>, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//returns texture dimension
	std::pair<int, int> getDimensions(std::string id);

	//modulate texture color
	void modulateColor(std::string id, Uint8 red, Uint8 green, Uint8 blue);

	void clean();
	void removeTexture(std::string id);

private:
	bool loadMetaData(std::string file, std::vector<std::vector<SDL_Rect>> &frameData);

private:
	//for creating text texture
	std::string mFontFile;
	std::unique_ptr<TextTextureManager> mTextManager;

	std::map<std::string, std::unique_ptr<Texture>> mTextureMap;
};
