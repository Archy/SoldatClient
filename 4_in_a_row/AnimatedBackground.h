#pragma once
#include "GameObject.h"

//scrolling background
class AnimatedBackground :
	public GameObject
{
public:
	AnimatedBackground();
	~AnimatedBackground();

	//load object basic data
	virtual void load(std::unique_ptr<LoaderParams> const &pParams);

	virtual void draw();
	virtual void update();
	virtual void clean();

private:
	//struct to keep data of shifted background
	//both dest and src rectangles
	struct animRect
	{
		float srcX;
		float destX;
		float srcWidth;
		float destWidth;
	};

private:
	//reset left and right rectangles
	void resetRect();
	//shift since prev call
	float shift();

private:
	//animation data
	int mAnimSpeed;
	int mTextureWidth;
	int mTextureHeight;
	
	animRect mLeftRect;
	animRect mRightRect;

	//for counting shift
	int mTicksPrev;
	int mTicksActual;
	
};