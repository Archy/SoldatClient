#include "Board.h"
#include <fstream>

Board::Board()
{
	init();
}

Board::~Board()
{
}

void Board::draw()
{
	//render each brick
	for (int i = 0; i < sHEIGHT; ++i)
	{
		for (int j = 0; j < sWIDTH; ++j)
		{
			if (mTab[i][j])
			{
				int dx = (int)Game::Instance()->getGameWidth() / sWIDTH;
				int dy = (int)(Game::Instance()->getGameHight() - mPosition.getY()) / sHEIGHT;

				SDL_Rect dest{j*dx, i*dy + (int)mPosition.getY(), dx, dy};
				std::pair<int, int> textureDim = Game::Instance()->getTextureMenager()->getDimensions(mTextureID);
				SDL_Rect src{0,0, textureDim.first, textureDim.second };
				Game::Instance()->getTextureMenager()->draw(mTextureID, src, dest, Game::Instance()->getRenderer());
			}
		}
	}
}

void Board::update()
{
	//no update
	//board does not change over time
}

void Board::clean()
{
}

bool Board::getBrick(int x, int y)
{
	if (x < 0 || sWIDTH <= x || y < 0 || sHEIGHT <= y)
		return false;

	return mTab[y][x];
}

void Board::init()
{
	//load board from external file
	std::ifstream file;
	file.open("assets/board.txt");
	if (!file.good())
	{
		std::cout << "Couldnt load board data\n";
		return;
	}

	int x;
	for (int i = 0; i < sHEIGHT; ++i)
	{
		for (int j = 0; j < sWIDTH; ++j)
		{
			file >> x;
			mTab[i][j] = (x == 1);
		}
	}
	file.close();

}
