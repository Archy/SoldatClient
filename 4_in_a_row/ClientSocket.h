#pragma once
#include "Socket.h"
#include <string>

class ClientSocket :
	public Socket
{
public:
	ClientSocket();
	~ClientSocket();

	//init client -> severs IP and port
	bool init(std::string remoteIP, const USHORT port);
	//send data of given size to server
	bool sendData(const char *data, int &size);

private:
	//init mLocalAddr and mRemoteAddr
	void createClient();

private:
	int mPort;
	char mServerIP[sIP_SIZE];
};

