#pragma once
#include "AnimatedObject.h"
#include "Board.h"
#include <memory>

class Soldier :
	public AnimatedObject
{
public:
	enum SoldierState {
		MOVE,
		STAY,
		DIE
	};

	enum SoldierAction {
		JUMP,
		FALL,
		NONE,
		SHOOT
	};

	enum SoldierDir {
		LEFT,
		RIGHT
	};

public:
	Soldier(int nr);
	~Soldier();

	virtual void update();
	virtual void draw();

	//set new state and update frame
	void setState(SoldierState state, SoldierAction action, 
		SoldierDir dir, bool animationReset);
	//set state based on data from sever
	void setState(char a);
	
	SoldierDir getDir() const { return mDir; }
	SoldierAction getAction() const { return mAction; }
	SoldierState getState() const { return mState; }
	char getNr() const { return mNumber; }

	//set soldier position
	void setX(float x) { mPosition.setX(x); }
	void setY(float y) { mPosition.setY(y); }

private:
	//start new animation
	void setFrames();

private:
	SoldierState mState;
	SoldierAction mAction;
	SoldierDir mDir;

	const char mNumber;

	bool mWasAnimationReseted;
};

