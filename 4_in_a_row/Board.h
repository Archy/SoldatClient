#pragma once
#include "GameObject.h"
#include "Game.h"
#include <string>

class Board :
	public GameObject
{
public:
	Board();
	~Board();

	virtual void draw();
	virtual void update();
	virtual void clean();

	//board data
	bool getBrick(int x, int y);
	int getRows() { return sHEIGHT; }
	int getCol() { return sWIDTH; }
	int getDY() { return static_cast<int>(mPosition.getY()); }
	int getBrickW() { return (int)Game::Instance()->getGameWidth() / sWIDTH; }
	int getBrickH() { return (int)(Game::Instance()->getGameHight() - mPosition.getY()) / sHEIGHT; }

private:
	void init();

private:
	constexpr static const int sWIDTH = 16;
	constexpr static const int sHEIGHT = 19;
	bool mTab[sHEIGHT][sWIDTH];
};

