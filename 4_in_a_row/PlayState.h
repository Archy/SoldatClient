#pragma once
#include "MenuState.h"
#include "GameStateCreator.h"
#include <SDL.h>
#include "ClientSocket.h"
#include "Player.h"
#include "LoopedCounter.h"
#include "BulletHandler.h"

class Soldier;

class PlayState :
	public MenuState
{
public:
	~PlayState();

	virtual bool EnterState();
	virtual bool ExitState();

	virtual void update();
	virtual void render();

private:
	//communication with server
	void communicate();
	//first packet received from server
	//-> set players number
	void firstReceipt(const int size);

private:
	friend class PlayStateCreator;
	PlayState(int stateID);

	//add callbacks functions to callbacks vector
	virtual void pushCallbacks();

	std::unique_ptr<ClientSocket> mClient;
	std::shared_ptr<Player> mPlayer;
	std::vector<std::shared_ptr<Soldier>> mSoldiers;
	std::shared_ptr<BulletHandler> mBulletHandler;

	bool mStarted;
	bool mFinished;
	int SOLDIER_NR;

	//packet numeration
	LoopedCounter lastSend;
	LoopedCounter lastReceived;
};


class PlayStateCreator : public GameStateCreator
{
	virtual GameState* createGameObject(const int id) const
	{
		return new PlayState(id);
	}
};