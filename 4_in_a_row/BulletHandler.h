#pragma once
#include "AnimatedObject.h"
#include <vector>
#include <SDL.h>

class BulletHandler :
	public AnimatedObject
{
private:
	//struct that keep data of every existing bullet
	struct Bullet
	{
		int mX; 
		int mY;
		SDL_RendererFlip mFlip;
		
		Bullet(int x, int y, SDL_RendererFlip pFlip) :
			mX(x), mY(y), mFlip(pFlip)
		{
		}

		void set(int x, int y, SDL_RendererFlip pFlip)
		{
			mX = x;
			mY = y;
			mFlip = pFlip;
		}
	};

public:
	BulletHandler();
	~BulletHandler();

	virtual void draw();

	//set bullets data based on data received from sever 
	void setData(char *tab);

private:
	std::vector<Bullet> mBullets;

	static const int sBulletWidth = 16;
	static const int sBulletHeight = 8;
};

