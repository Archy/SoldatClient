#include "MySDL.h"
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <iostream>


MySDL::MySDL() :
	mWindow(nullptr),
	mRenderer(nullptr)
{
}

MySDL::~MySDL()
{
	mWindow.reset();
	mRenderer.reset();

	//Quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();

	std::cout << "SDL cleaned succesfully" << '\n';

}


bool MySDL::init(std::string title, int xpos, int ypos,
	int width, int height, bool fullscreen)
{
	int flags = 0;

	//Initialization flag
	bool success = true;

	if (fullscreen)
	{
		flags = SDL_WINDOW_FULLSCREEN;
	}

	//Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "SDL could not initialize!SDL Error : " << 
								SDL_GetError() << '\n';
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			std::cout << "Warning: Linear texture filtering not enabled!"
								<< '\n';
		}

		//Create window
		mWindow = std::shared_ptr<SDL_Window>(
			SDL_CreateWindow(title.c_str(), xpos, ypos, width, height, flags),
			[](SDL_Window* wind) {SDL_DestroyWindow(wind); });
		if (mWindow == NULL)
		{
			std::cout << "Window could not be created! SDL Error: " 
								<< SDL_GetError() << '\n';
			success = false;
		}
		else
		{
			//Create renderer for window
			mRenderer = std::shared_ptr<SDL_Renderer>(
				SDL_CreateRenderer(mWindow.get(), -1, SDL_RENDERER_ACCELERATED),
				[](SDL_Renderer *render) { SDL_DestroyRenderer(render); });
			if (mRenderer == NULL)
			{
				std::cout << "Renderer could not be created! SDL Error: " 
									<< SDL_GetError() << '\n';
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(mRenderer.get(), 0, 0, 0, 255);

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					std::cout << "SDL_image could not initialize! SDL_image Error: "
										<< IMG_GetError() << '\n';
					success = false;
				}

				//Initialize SDL_ttf
				if (TTF_Init() == -1)
				{
					std::cout << "SDL_ttf could not initialize! SDL_ttf Error: " <<
						TTF_GetError() << "\n";
					success = false;
				}
			}
		}
	}

	return success;
}


void MySDL::clean()
{
}