#include "Timer.h"



Timer::Timer()
{
}


Timer::~Timer()
{
}

void Timer::start()
{
	mTime = SDL_GetTicks();
	mPrevTime = SDL_GetTicks();
}

void Timer::update()
{
	mPrevTime = mTime;
	mTime = SDL_GetTicks();
}

float Timer::getDT()
{
	return (float)(mTime - mPrevTime)/1000.0f;
}
