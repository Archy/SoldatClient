#pragma once
#include "MenuState.h"
#include "GameStateCreator.h"

class PauseState :
	public MenuState
{
public:
	~PauseState();

	virtual bool EnterState();
	virtual bool ExitState();

private:
	friend class PauseStateCreator;
	PauseState(int stateID);

	//add callbacks functions to callbacks vector
	virtual void pushCallbacks();

	//static callback functions
	static void exitFunction();
	static void resumeFunction();
	static void soundFunction();
};


class PauseStateCreator : public GameStateCreator
{
	virtual GameState* createGameObject(const int id) const
	{
		return new PauseState(id);
	}
};