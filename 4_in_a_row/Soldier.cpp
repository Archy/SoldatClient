#include "Soldier.h"
#include <SDL.h>
#include "Game.h"

Soldier::Soldier(int nr) :
	mState(SoldierState::STAY), mAction(SoldierAction::NONE),
	mDir(SoldierDir::RIGHT), mNumber(nr), mWasAnimationReseted(false)
{
	setFrames();
	if (mNumber > 3)
	{
		std::cout << "WRONG SOLDIER NUMBER: " << mNumber << "\n";
	}
}


Soldier::~Soldier()
{
}

void Soldier::update()
{
	if (mAction == Soldier::SHOOT && mAnimFinished==true)
	{
		//mAction = Soldier::NONE;
		setFrames();
	}

//animation
	AnimatedObject::update();
}

void Soldier::draw()
{
//set soldier color modulation
	switch (mNumber)
	{
	case 0:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 255, 215, 0);
		break;
	case 1:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 255, 75, 75);
		break;
	case 2:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 25, 255, 25);
		break;
	case 3:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 75, 75, 255);
		break;
	default:
		Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 0, 0, 0);
		break;
	}

	//AnimatedObject::draw();
	SDL_Rect destRect{ static_cast<int>(mPosition.getX()),
		static_cast<int>(mPosition.getY()),
		mWidth, mHeight };

	Game::Instance()->getTextureMenager()->drawFrame(mTextureID, destRect,
		mCurrentFrame, mAnimationRow, Game::Instance()->getRenderer(), mFlip);

//reset texture color modulation
	Game::Instance()->getTextureMenager()->modulateColor(mTextureID, 255, 255, 255);
}

void Soldier::setState(SoldierState state, SoldierAction action, 
	SoldierDir dir, bool animationReset)
{
	static bool change = false;

	if (mDir != dir)
	{
		mDir = dir;
		switch (mDir)
		{
		case Soldier::LEFT:
			mFlip = SDL_FLIP_HORIZONTAL;
			break;
		case Soldier::RIGHT:
			mFlip = SDL_FLIP_NONE;
			break;
		default:
			break;
		}
	}

	//update state if necessary
	if (change && action != mAction)
	{
		bool pSetFrames = !(action==SoldierAction::FALL &&  mAction==SoldierAction::JUMP);

		mState = state;
		mAction = action;
		if(pSetFrames)
			setFrames();
		change = false;
	}
	else if (change && state != mState && mAction==SoldierAction::NONE)
	{
		mState = state;
		mAction = action;
		setFrames();
		change = false;
	}

	if (animationReset && !mWasAnimationReseted)
	{
		setFrames();
	}
	mWasAnimationReseted = animationReset;

	if (action != mAction || state != mState)
		change = true;

}

void Soldier::setState(char a)
{
	//	xx		x		xx			xx			x
	//	nr		dir		state		action		animation reset

//check if nr matches soldier nr
	if (mNumber != (a & 0xc0) >> 6)
	{
		//std::cout << "\n\tWRONG SOLDIER NUMBER WHEN SETTING STATE!!!\t" << 
		//	((a & 0xc0) >> 6) <<"\n";
		return;
	}

	a &= 0x3F;	//reset nr bits

	char dir = (a & 0x20) >> 5;
	char state = (a & 0x18) >> 3;
	char action = (a & 0x06) >> 1;
	bool animationReset = (a & 0x01) == 1;

	Soldier::SoldierDir soldierDir = (dir == 1 ?
		Soldier::SoldierDir::RIGHT : Soldier::SoldierDir::LEFT);

	Soldier::SoldierState soldierState;
	switch (state)
	{
	case 0:
		soldierState = Soldier::SoldierState::STAY;
		break;
	case 1:
		soldierState = Soldier::SoldierState::MOVE;
		break;
	case 3:
		soldierState = Soldier::SoldierState::DIE;
		break;
	}

	Soldier::SoldierAction soldierAction;
	switch (action)
	{
	case 0:
		soldierAction = Soldier::SoldierAction::NONE;
		break;
	case 1:
		soldierAction = Soldier::SoldierAction::JUMP;
		break;
	case 2:
		soldierAction = Soldier::SoldierAction::FALL;
		break;
	case 3:
		soldierAction = Soldier::SoldierAction::SHOOT;
		break;
	}

	setState(soldierState, soldierAction, soldierDir, animationReset);
}

void Soldier::setFrames()
{
	//start new soldiers animation

	mAnimFinished = false;
	mLoopAnim = true;

	switch (mState)
	{
	case Soldier::MOVE:
		mFramesNumber = 15;
		mAnimationRow = 1;
		mCurrentFrame = 0;
		break;
	case Soldier::STAY:
		mFramesNumber = 6;
		mAnimationRow = 0;
		mCurrentFrame = 0;
		break;
	case Soldier::DIE:
		mFramesNumber = 6;
		mAnimationRow = 4;
		mCurrentFrame = 0;
		mLoopAnim = false;
		mAnimSpeed = 10;
		break;
	default:
		break;
	}

	switch (mAction)
	{
	case Soldier::JUMP:
		mFramesNumber = 7;
		mAnimationRow = 3;
		mCurrentFrame = 0;
		mLoopAnim = false;
		break;
	case Soldier::FALL:
		mFramesNumber = 7;
		mAnimationRow = 3;
		mCurrentFrame = 6;
		mAnimFinished = true;
		mLoopAnim = false;
		break;
	case Soldier::SHOOT:
		mFramesNumber = 6;
		mAnimationRow = 2;
		mCurrentFrame = 0;
		mLoopAnim = false;
		break;
	case Soldier::NONE:
	default:
		break;
	}

	switch (mDir)
	{
	case Soldier::LEFT:
		mFlip = SDL_FLIP_HORIZONTAL;
		break;
	case Soldier::RIGHT:
		mFlip = SDL_FLIP_NONE;
		break;
	default:
		break;
	}
}
