#include "BulletHandler.h"
#include "Game.h"

BulletHandler::BulletHandler()
{
	mBullets.reserve(50);

	mLoopAnim = false;
	mAnimFinished = true;
	mAnimationRow = 5;
	mCurrentFrame = 2;
}

BulletHandler::~BulletHandler()
{
}


void BulletHandler::draw()
{
	SDL_Rect destRect{ 0, 0, sBulletWidth, sBulletHeight };

	//draw every bullet with proper flipping
	for (auto& b : mBullets)
	{
		destRect.x = b.mX;
		destRect.y = b.mY;

		Game::Instance()->getTextureMenager()->drawFrame(mTextureID, destRect,
			mCurrentFrame, mAnimationRow, Game::Instance()->getRenderer(), b.mFlip);
	}
}

void BulletHandler::setData(char * tab)
{
	char size = tab[0];
	++tab;
	//ensure vector have enough elements
	if (size > static_cast<unsigned char>(mBullets.size()))
		mBullets.reserve(2 * size);

	//if there are any bullets on board
	if (size > 0)
	{
		int *data = (int*)tab;
		mBullets.clear();
		for (int i = 0; i < size; ++i)
		{
			int x = data[i];
			int y = data[i];

			x &= 0x0000FFFF;
			y >>= 16;
			y &= 0x00007FFF;

			if((data[i] & 0x80000000) == 0)
				mBullets.push_back(Bullet(x, y, SDL_FLIP_NONE));
			else
				mBullets.push_back(Bullet(x, y, SDL_FLIP_HORIZONTAL));
		}
	}
	else
	{
		mBullets.clear();
	}
}
