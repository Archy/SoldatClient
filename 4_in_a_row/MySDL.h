#pragma once
#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <memory>
#include <string>

//manages SDL
class MySDL
{
public:
	MySDL();
	~MySDL();

	bool init(std::string title, int xpos, int ypos, 
		int width, int height, bool fullscreen);

	void clean();

	//returns rendering context
	std::shared_ptr<SDL_Renderer> getRenderer() const { return mRenderer; }
	//return window
	std::shared_ptr<SDL_Window> getWindow() const { return mWindow; }

private:
	//window for drawing to
	std::shared_ptr<SDL_Window> mWindow;
	//render target for 2D Accelerated Rendering
	std::shared_ptr<SDL_Renderer> mRenderer;
};