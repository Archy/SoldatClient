#pragma once

//used for packets numeration
class LoopedCounter
{
public:
	LoopedCounter();
	~LoopedCounter();

	//compare 2 looped number.
	//255 < 0 = true !!!
	bool operator<(const unsigned char& other);

	//increase and return
	unsigned char getNext();
	//set new value
	void setValue(unsigned char x);
private:
	unsigned char mx;

};
