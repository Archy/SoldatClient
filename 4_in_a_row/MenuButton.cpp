#include "MenuButton.h"
#include "LoaderParams.h"
#include "Game.h"
#include <utility>



MenuButton::MenuButton()
{
}

MenuButton::~MenuButton()
{
}


void MenuButton::load(std::unique_ptr<LoaderParams> const &pParams)
{
	mCallbackID = pParams->getCallbackID();
	mText = pParams->getText();
	m_callback = nullptr;
	GameObject::load(pParams);

	mMouseOver = false;
	mClicked = false;
}


void MenuButton::draw()
{
	std::pair<int, int> TextureDim = Game::Instance()->
		getTextureMenager()->getDimensions(mTextureID);
	std::pair<int, int> TextDim ( Game::Instance()->
		getTextureMenager()->getDimensions(mText));

	//basic button texture
	SDL_Rect srcRect{ 0, 0, static_cast<int>(TextureDim.first / 2),
		TextureDim.second };
	SDL_Rect destRect{ static_cast<int>(mPosition.getX()), 
		static_cast<int>(mPosition.getY()),
		mWidth, mHeight };
	
	//text texture source rectangle = whole text texture
	SDL_Rect TextSrcRect{ 0, 0, TextDim.first, TextDim.second };

	//text texture destination rectangle = centered at the button texture
	int textWidth = static_cast<int>(0.7 * mHeight * 
		TextDim.first / TextDim.second );
	SDL_Rect TextDestRect{ 
		static_cast<int>(mPosition.getX() + (mWidth - textWidth) / 2),
		static_cast<int>(mPosition.getY() + 0.1 * mHeight),
		textWidth, static_cast<int>(0.7 * mHeight) };

	if (mClicked)
	{
		//change button texture to highlighted
		srcRect.x = static_cast<int>(TextureDim.first / 2); 
		TextDestRect.y = static_cast<int>(mPosition.getY() + 0.2 * mHeight);
	}
	else if (mMouseOver)
	{
		//move text down => giving illusion of button being clicked
		srcRect.x = static_cast<int>(TextureDim.first / 2);
	}

		Game::Instance()->getTextureMenager()->draw(mTextureID,
			srcRect, destRect, Game::Instance()->getRenderer());
		Game::Instance()->getTextureMenager()->draw(mText,
			TextSrcRect, TextDestRect, Game::Instance()->getRenderer());
}

void MenuButton::update()
{
	std::shared_ptr<const Vector2D> pMousePos = Game::Instance()->
		getInputHandler()->getMousePosition();
	//if mouse is over
	if (pMousePos->getX() < (mPosition.getX() + mWidth) && 
		pMousePos->getX() > mPosition.getX()
		&& pMousePos->getY() < (mPosition.getY() + mHeight) 
		&& pMousePos->getY() > mPosition.getY())
	{
		mMouseOver = true;
		//button clicked
		if (Game::Instance()->
			getInputHandler()->getMouseButtonState(InputHandler::LEFT))
		{
			mClicked = true;
		}
		//button released
		else if (mClicked && !Game::Instance()->getInputHandler()->
			getMouseButtonState(InputHandler::LEFT))
		{
			if (m_callback != nullptr)
				m_callback();
			else
				std::cout << "Callback is not set!!!\n";
			mClicked = false;
		}
	}
	else
	{
		//mouse not over
		mMouseOver = false;
		if (!Game::Instance()->getInputHandler()->
			getMouseButtonState(InputHandler::LEFT))
		{
			mClicked = false;
		}
	}
}

void MenuButton::clean()
{

}