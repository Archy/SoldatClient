#include "PlayState.h"
#include "Game.h"
#include "GameObject.h"
#include "LoaderParams.h"
#include "AnimatedObject.h"
#include "AnimatedBackground.h"

#include "Soldier.h"
#include "Board.h"

PlayState::PlayState(int stateID) :
	MenuState(stateID), mStarted(false), mFinished(false)
{
}


PlayState::~PlayState()
{
}


bool PlayState::EnterState()
{
// TEXTURES
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/clouds.png", "cloud",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("cloud"); // push into list
	}
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/brick.png", "brick",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("brick"); // push into list
	}
	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/wait.png", "wait",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("wait"); // push into list
	}

	if (Game::Instance()->getTextureMenager()->loadTexture(
		"assets/reloading.png", "reload",
		Game::Instance()->getRenderer()))
	{
		mTextureIDList.push_back("reload"); // push into list
	}
	if (Game::Instance()->getTextureMenager()->loadSpriteShit(
		"assets/gameover.png", "gameover",
		Game::Instance()->getRenderer(), NULL, "assets/gameover.txt"))
	{
		mTextureIDList.push_back("gameover"); // push into list
	}
	if (Game::Instance()->getTextureMenager()->loadSpriteShit(
		"assets/you_win.png", "win",
		Game::Instance()->getRenderer(), NULL, "assets/win.txt"))
	{
		mTextureIDList.push_back("win"); // push into list
	}
	
	SDL_Color soldierTransparent{ 184, 72, 184, 255 };
	if (Game::Instance()->getTextureMenager()->loadSpriteShit(
		"assets/soldier.png", "soldier", Game::Instance()->getRenderer(),
		&soldierTransparent, "assets/meta.txt"))
	{
		mTextureIDList.push_back("soldier"); // push into list
	}


// OBJECTS 
	
	std::shared_ptr<AnimatedObject> gameOver1 = std::make_shared<AnimatedObject>();
	gameOver1->load(std::unique_ptr<LoaderParams>(new LoaderParams(130, 90,
		190, 30, "gameover", 2, 3)));
	mStateObjects.push_back(gameOver1);



	std::shared_ptr<AnimatedBackground> background = std::make_shared<AnimatedBackground>();
	background->load(std::unique_ptr<LoaderParams>(new LoaderParams(0, 0,
		Game::Instance()->getGameWidth(), Game::Instance()->getGameHight(), "cloud", 1, 30)));
	mStateObjects.push_back(background);

	std::shared_ptr<Board> board = std::make_shared<Board>();
	board->load(std::unique_ptr<LoaderParams>(new LoaderParams(0, 5,
		Game::Instance()->getGameWidth(), Game::Instance()->getGameHight()-5, "brick")));
	mStateObjects.push_back(board);

	std::shared_ptr<BulletHandler> bullets = std::make_shared<BulletHandler>();
	bullets->load(std::unique_ptr<LoaderParams>(new LoaderParams(0,
		0, 16, 8, "soldier", 1, 12)));
	mStateObjects.push_back(bullets);
	mBulletHandler = bullets;

	//player
	mPlayer = std::make_shared<Player>();
	mPlayer->load(std::unique_ptr<LoaderParams>(new LoaderParams(0, 0, 
		board->getBrickW(), board->getBrickH(), "soldier", 6, 12)));
	mStateObjects.push_back(mPlayer);

//Create client
	mClient = std::make_unique<ClientSocket>();
	
	const char* ip = "192.168.0.12";
	char IP[17];
	std::cin.getline(IP, 16);
	mClient->init(IP, 44666);

	pushCallbacks();
	assignCallbacks();

	return true;
}


bool PlayState::ExitState()
{
	std::cout << "exiting PlayState\n";
	return true;
}


//updates all objects
void PlayState::update()
{
	Sleep(2);

	mPlayer->update();
	communicate();

	for (auto& it: mStateObjects)
	{
		it->update();
	}


	if (mFinished &&
		Game::Instance()->getInputHandler()->isKeyDown(SDL_SCANCODE_RETURN))
	{
		Game::Instance()->getStateMachine()->changeState(
			StateMachine::GameStatesIDs::MainMenuState);
	}
}

void PlayState::render()
{
	if (mStarted)
	{
		MenuState::render();
	}
	else
	{
		//game is not started - waiting for players

		SDL_Rect dest{0,0, Game::Instance()->getGameWidth(), Game::Instance()->getGameHight()};
		std::pair<int, int> dim = Game::Instance()->getTextureMenager()->getDimensions("wait");
		SDL_Rect src{0,0, dim.first, dim.second };

		Game::Instance()->getTextureMenager()->draw("wait", src, dest, Game::Instance()->getRenderer());
	}
}

void PlayState::communicate()
{
	unsigned char packetNr = lastSend.getNext();
	char a[2] = { reinterpret_cast<char&>(packetNr),
		mPlayer->getStateToSend() };
	int size = 2;
	const int ADITIONAL_PACKET_DATA = 1;
	mClient->sendData(a, size);	//send players data to server

	//read data to update soldiers
	char buff[512];
	size = 512;
	char senderIP[16];
	USHORT senderPort;

	while(mClient->readData(buff, size, senderIP, senderPort) && size > 0)
	{
		if (size > 1)
		{
			if (!mStarted)
			{
				firstReceipt(size);
			}

			mStarted = true;
			unsigned char receivedNr = reinterpret_cast<unsigned char&>(buff[0]);
			if (lastReceived < receivedNr) //discard outdated packets
			{
				const int soldierData = 9;

				for (int i = 0; i < SOLDIER_NR; ++i)
				{
					mSoldiers[i]->setState(buff[soldierData*i + ADITIONAL_PACKET_DATA]);
					float *f = (float*)&buff[1 + soldierData*i + ADITIONAL_PACKET_DATA];
					mSoldiers[i]->setX(*f);
					f = (float*)&buff[5 + soldierData*i + ADITIONAL_PACKET_DATA];
					mSoldiers[i]->setY(*f);
				}

				char info = buff[ADITIONAL_PACKET_DATA + soldierData*SOLDIER_NR];
				if (info == 7 || info == 6)
				{
					if (!mFinished)
					{
						mFinished = true;
						if (info == 7)
						{
							//victory
							std::shared_ptr<AnimatedObject> gameWin = std::make_shared<AnimatedObject>();
							gameWin->load(std::unique_ptr<LoaderParams>(new LoaderParams(20, 90,
								600, 100, "win", 1, 1)));
							mStateObjects.push_back(gameWin);
						}
						else
						{
							//loss
							std::shared_ptr<AnimatedObject> gameOver1 = std::make_shared<AnimatedObject>();
							gameOver1->load(std::unique_ptr<LoaderParams>(new LoaderParams(130, 90,
								190, 30, "gameover", 2, 3)));
							mStateObjects.push_back(gameOver1);

							std::shared_ptr<AnimatedObject> gameOver2 = std::make_shared<AnimatedObject>();
							gameOver2->load(std::unique_ptr<LoaderParams>(new LoaderParams(320, 90,
								190, 30, "gameover", 2, 3)));
							mStateObjects.push_back(gameOver2);
						}
					}
				}
				else
				{
					mPlayer->setInfo(info);
				}
				
				mBulletHandler->setData(&(buff[ADITIONAL_PACKET_DATA + soldierData*SOLDIER_NR + 1]));


				lastReceived.setValue(receivedNr);
			}
		}
	}
}

void PlayState::firstReceipt(const int size)
{
	SOLDIER_NR = (size - 3) / 9;

	std::cout << "PLAYERS: " << SOLDIER_NR;

	int soldierPositions[8] = { 40, 5, Game::Instance()->getGameWidth() - 80, 5 };
	//soldiers
	for (int i = 0; i < SOLDIER_NR; ++i)
	{
		std::shared_ptr<Soldier> s = std::make_shared<Soldier>(i);
		s->load(std::unique_ptr<LoaderParams>(new LoaderParams(soldierPositions[2 * i],
			soldierPositions[2 * i + 1], 40, 50, "soldier", 6, 12)));
		mStateObjects.push_back(s);
		mSoldiers.push_back(s);
	}
}


void PlayState::pushCallbacks()
{
}