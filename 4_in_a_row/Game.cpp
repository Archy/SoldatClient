#include "Game.h"
#include <iostream>

std::shared_ptr<Game> Game::sInstance;
bool Game::sCreatedInstance = false;

Game::Game() :
	mRunning(false),
	mMySDL()
{
}

Game::~Game()
{
}


void Game::clean()
{
	
	mInputHandler->clean();
	mGameStateMachine->clean();
	mTextureMenager->clean();
	mMySDL->clean();
}

//get game instance
std::shared_ptr<Game> Game::Instance()
{
	if (!sCreatedInstance) {
		sCreatedInstance = true;
		sInstance = std::make_shared<Game>();
	}

	return Game::sInstance;
}

void Game::deleteInstance()
{

}


bool Game::init(const char* title, int xpos, int ypos, 
	int width, int height, bool fullscreen)
{
	bool initSuccess = false;

	// store the game width and height
	mGameWidth = width;
	mGameHeight = height;

	//set up SDL
	mMySDL = std::make_shared<MySDL>();
	initSuccess = mMySDL->init(title, xpos, ypos, width,
		height, fullscreen);
	if (initSuccess)
	{
		//game is running
		mRunning = true;

		//init input handler
		mInputHandler = std::make_shared<InputHandler>();

		//init textureManager
		mTextureMenager = std::make_shared<TextureMenager>();
		mTextureMenager->init();

		mTimer = std::make_shared<Timer>();
		mTimer->start();

		//init StateMachine
		mGameStateMachine = std::make_shared<StateMachine>();
		mGameStateMachine->init();
		mGameStateMachine->addState(StateMachine::MainMenuState);
		//TEST ONLY - create only playstate
		//mGameStateMachine->changeState(StateMachine::GameEndState);
	}
	return initSuccess;
}


void  Game::render()
{
	//clear rendering target
	SDL_RenderClear(mMySDL->getRenderer().get());
	//draw objects to render target
	mGameStateMachine->render();
	//show render target on screen
	SDL_RenderPresent(mMySDL->getRenderer().get());
}

void  Game::update()
{
	//update game state
	mTimer->update();
	mGameStateMachine->update();
}

void  Game::handleEvents()
{
	//handle user input
	mInputHandler->update();
}


SDL_Rect Game::getPlayRect()
{
	SDL_Rect playRect;
	playRect.x = 110;
	playRect.y = 60;
	playRect.w = 420;
	playRect.h = 420;

	return playRect;
}