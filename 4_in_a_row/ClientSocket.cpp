#include "ClientSocket.h"
#include <iostream>


ClientSocket::ClientSocket()
{
}


ClientSocket::~ClientSocket()
{
}



bool ClientSocket::init(std::string remoteIP, const USHORT port)
{
	strcpy(mServerIP, remoteIP.c_str());
	mPort = port;

	bool result;
	result = Socket::init(port);
	if (!result)
		return result;
	createClient();
	return true;
}

bool ClientSocket::sendData(const char * data, int & size)
{
	int result;
	result = sendto(mSock, data, size, 0, (SOCKADDR *)&mRemoteAddr, sizeof(mRemoteAddr));
	return result != SOCKET_ERROR;
}

void ClientSocket::createClient()
{
	char localIP[sIP_SIZE];
	IN_ADDR addr;

	// set local IP address
	getLocalIP(localIP);          // get local IP
	inet_pton(AF_INET, localIP, &addr);
	mLocalAddr.sin_addr.S_un.S_addr = addr.S_un.S_addr;

	//set sever IP
	memset(&addr, 0, sizeof(addr));
	inet_pton(AF_INET, mServerIP, &addr);	//cstring to addr struct
	mRemoteAddr.sin_addr.S_un.S_addr = addr.S_un.S_addr;

}
